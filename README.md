# README #

### Summary ###

This program is designed to showcase use of threading. It implements a many to many producer/consumer relationship. In order to use the program you must supply a list of coma
separated files, as well as a list of coma seperated words. The usage is as follows

    java WordSearch files words

Once running the program will search through the files for the chosen words and upon finding a match will immediatly print out the word found, followed by the file it was found in.

### Development ###

This program was developed for a parallel and distributed computing class. It shows basic thread usage. As further concepts are explored they may be added to this repo.

### Contact ###
Please contact alf40k@gmail.com or ArthurLLunn@gmail.com with any questions