// file: WordSearch.java
// date: 2/19/15

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The WordSearch program is given a list of text file names and a list of target words.
 * The program examines all the words in each of the files. If a certain target word
 * appears in a certain file (any number of times), the program prints the target word
 * and the file name. A word is defined to be a maximal length sequence of letters A
 * through Z and a through z (set of letters). Each word is separated from the next by
 * one or more non-letter characters (anything that is not a letter). Uppercase/lowercase
 * does not matter; for example, HELLO and hello and HeLLo are the same word.
 * (description taken from
 * <a href="https://cs.rit.edu/~rwd/csci251/projects/project1/index.html#system">Project description</a>})
 * @author Arthur Lunn | all3187
 * @version 1.1
 * @since 2/19/16
 */
public class WordSearch{
    // Error strings
    final static String invalidArgs = "invalid number of args\n" +
            " Usage: java WordSearch <files> <words>";
    final static String invalidFiles = "invalid <files> arg\n" +
            " <files> must be a C.S. list of 1+ file names with no whitespace";
    final static String invalidWords = "invalid <words> arg\n" +
            " <words> must be a C.S. list of 1+ words with no whitespace";

    /**
     * Takes a list of one or more text file names, separated by commas
     * with no whitespace and a list of one or more target words,
     * separated by commas with no whitespace. If the command line
     * does not have 2 arguments or if either of the arguments is erroneous
     * the program will print a usage message to standard error and exit.
     *
     * @param args files - a list of file names words - a list of words
     */
    public static void main(String[] args) {
        // Lock for file synchronization
        final Object searchLock = new Object();
        // Array of file names
        String[] fileStrings;
        // Array of words to search for
        String[] words;
        // Array of scanners for each file
        Scanner[] files;
        // A table holding a queue of file names; uses search words for keys
        // Each queue holds the names of the files the <key> words were found in
        Hashtable<String, ConcurrentLinkedQueue<String>> printQueue;
        printQueue = new Hashtable<String, ConcurrentLinkedQueue<String>>();
        // Indicators to show when thread group 1 is finished
        AtomicInteger finishedCount = new AtomicInteger(0);
        AtomicInteger finalCount = new AtomicInteger(0);
        // Arrays of both of the thread groups
        Thread[] tg1;
        Thread[] tg2;
        // Print error if the incorrect number of arguments given
        if ( args.length != 2){
            System.err.println(invalidArgs);
            System.exit(-1);
        }
        // Split the files argument into file strings
        fileStrings = args[0].split(",");
        // Create scanners for each of the files handling exceptions
        files = new Scanner[fileStrings.length];
        finalCount.set(fileStrings.length);
        for( int i = 0; i < fileStrings.length; i++){
            try{
                files[i] = new Scanner( new File(fileStrings[i]) );

            }catch (FileNotFoundException e){
                System.err.println(invalidFiles);
                System.exit(-2);
            }
        }
        // Make a list of all the words
        words = args[1].split(",");
        // Check that each "word" is an actual word and add them to the table
        for( int i = 0; i < words.length; i++) {
            if(! words[i].matches("[a-zA-Z]+")) {
                System.err.println(invalidWords);
                System.exit(-2);
            }
            else{
                words[i] = words[i].toLowerCase();
                printQueue.put(words[i], new ConcurrentLinkedQueue<String>());
            }
        }
        // Put all of the threads into their own arrays
        tg2 = new Thread[words.length];
        tg1 = new Thread[files.length];
        // Create and start all of the threads
        for(int i = 0; i < words.length; i++){
            tg2[i] = (new Thread( new Thread2(words[i], printQueue.get(words[i]), searchLock ,
                                              finishedCount, finalCount) ));
            tg2[i].start();
        }
        for(int i = 0; i < files.length; i++){
            tg1[i] = (new Thread( new Thread1(files[i], fileStrings[i], deepCopy(words),
                                              printQueue, searchLock, finishedCount) ));
            tg1[i].start();
        }
    }

    /**
     * Basic method for creating a deep copy of an array.
     * @param src The array to be copied
     * @return A deep copy of the src array
     */
    @SuppressWarnings("ManualArrayCopy")
    private static String[] deepCopy(String[] src){
        String[] copy = new String[src.length];
        for (int i = 0; i < src.length; i++){
            copy[i] = src[i];
        }
        return copy;
    }
}
/**
 * Each thread in thread group one is given a file to search. Upon finding
 * one of the specified words in the file it is assigned it will
 * add the file name onto the queue of the corresponding Thread2.
 **/
class Thread1 implements Runnable{
    final private Object searchLock;
    private String fileS;
    private Scanner file;
    private String[] words;
    private AtomicInteger finishedCount;
    Hashtable<String, ConcurrentLinkedQueue<String>> printQueue;
    public Thread1(Scanner _file, String _fileS, String[] _words,
                   Hashtable<String, ConcurrentLinkedQueue<String>> _printQueue,
                   Object _searchLock,  AtomicInteger _finishedCount){
        words = _words;
        file = _file;
        fileS = _fileS;
        printQueue = _printQueue;
        searchLock = _searchLock;
        finishedCount = _finishedCount;
    }

    /**
     * Run method for thread group 1. Searches each line of the file for each word.
     */
    public void run(){
        String lineT;
        // Search every line of the file for the given word
        while(file.hasNextLine()){
            lineT = file.nextLine().toLowerCase();
            for(int i = 0; i < words.length; i++){
                if( ( words[i] != null ) && ( lineT.matches(".*\\b"+words[i]+"\\b.*") ) ){
                    printQueue.get(words[i]).add(fileS);
                    words[i] = null;
                }
            }
        }
        // Finally notify other threads
        synchronized (searchLock) {
            searchLock.notifyAll();
            finishedCount.incrementAndGet();
        }
    }
}

/**
 * Each thread in thread two is given a word to watch for. Whenever a
 * file name is added onto the corresponding queue this thread is
 * responsible for printing out information on the file. Once a NUL ("\0")
 * value is found the thread will stop.
 */
class Thread2 implements Runnable{

    private String word;
    private ConcurrentLinkedQueue<String> wordQ;
    final private Object searchLock;
    private AtomicInteger finishedCount;
    private AtomicInteger finalCount;


    /**
     * Main constructor for thread group 2.
     *
     * @param _word The word that this thread is responsible for
     * @param _wordQ The list of files that this word was found in
     * @param _finishedCount Shows how many thread group 1 threads finished
     * @param _finalCount Shows how many thread group 1 threads total
     * @param _searchLock The lock for synchronizing the methods
     */
    public Thread2(String _word, ConcurrentLinkedQueue<String> _wordQ,
                   Object _searchLock, AtomicInteger _finishedCount,
                   AtomicInteger _finalCount){
        word = _word;
        wordQ = _wordQ;
        searchLock = _searchLock;
        finishedCount = _finishedCount;
        finalCount = _finalCount;
    }

    /**
     * Run method for thread group 2. Responsible for taking all of the
     * files from the the file list and printing the appropriate message.
     */
    public void run(){
        while( ( !( wordQ.isEmpty() ) || ( finalCount.get() != finishedCount.get() ) ) ) {
            if( !( wordQ.isEmpty() ) ){
                System.out.println(word + " " + wordQ.poll());
            }
            if( finalCount.get() != finishedCount.get() ) {
                synchronized (searchLock) {
                    try {
                        searchLock.notifyAll();
                        searchLock.wait();
                    } catch (InterruptedException e) {
                        // catch this
                    }
                }
            }
        }
        synchronized (searchLock) {
            searchLock.notifyAll();
        }
    }
}